<?php

class EnvironmentRequest implements JsonSerializable {

	private string $name;
	private array $values;

	public function __construct( string $name ) {
		$this->name = $name;

		$this->values = [];
	}

	public function withBranch( string $branch ): EnvironmentRequest {
		$this->setCoreValue( "branch", $branch );
		return $this;
	}

	public function withIngress( string $ingress ): EnvironmentRequest {
		$this->setCoreValue( "ingress", $ingress );
		return $this;
	}

	public function withAdminUser( string $admin ): EnvironmentRequest {
		$this->setCoreValue( "user", $admin );
		return $this;
	}

	public function withDefaultPassword( string $pass ): EnvironmentRequest {
		$this->setCoreValue( "password", $pass );
		return $this;
	}

	public function withCoreRefs( array $refs ): EnvironmentRequest {
		$this->setCoreValue( "patches", $refs );
		return $this;
	}

	public function withMainPageText( string $text ): EnvironmentRequest {
		$this->setCoreValue( "mainPageText", $text );
		return $this;
	}

	public function useInstantCommons( bool $use ): EnvironmentRequest {
		$this->setCoreValue( "useInstantCommons", $use );
		return $this;
	}

	public function withLanguage( string $language ): EnvironmentRequest {
		$this->setCoreValue( "language", $language );
		return $this;
	}

	public function useProxy( bool $use ): EnvironmentRequest {
		$this->setCoreValue( "useProxy", $use );
		return $this;
	}

	public function buildDocs( bool $build ): EnvironmentRequest {
		$this->setCoreValue( "buildDocs", $build );
		return $this;
	}

	public function useTempUser( bool $use ): EnvironmentRequest {
		$this->setCoreValue( "useTempUser", $use );
		return $this;
	}

	public function withExtension( string $extension, string $branch, array $refs = [] ): EnvironmentRequest {
		return $this->withComponent( "extensions", $extension, $branch, $refs );
	}

	public function withSkin( string $skin, string $branch, array $refs = [] ): EnvironmentRequest {
		return $this->withComponent( "skins", $skin, $branch, $refs );
	}

	public function withModule( string $module, string $branch, array $refs = [] ): EnvironmentRequest {
		return $this->withComponent( "otherModules", $module, $branch, $refs );
	}

	public function useRepositoryPool( string $poolPath ): EnvironmentRequest {
		$this->values["reposCache"] = [ "use" => true, "wikiRepos" => $poolPath ];
		return $this;
	}

	private function setCoreValue( string $key, mixed $value ): void {
		$this->values["mediawikiCore"][$key] = $value;
	}

	private function withComponent( string $type, string $component, string $branch, array $refs ): EnvironmentRequest {
		$compConfig = [ "name" => $component, "enable" => true, "branch" => $branch ];
		if ( $refs ) {
			$compConfig += [ "patches" => $refs ];
		}
		$this->values[$type] ??= [];
		$this->values[$type][] = $compConfig;
		return $this;
	}

	public function jsonSerialize(): mixed {
		return [
			"name" => $this->name,
			"chartName" => "mediawiki",
			"values" => $this->values,
		];
	}
}
